//
// Created by zhu on 2017/12/6.
//


#include <iostream>

#include "Package.h"
#include "OvernightPackage.h"
#include "TwoDayPackage.h"

using namespace std;

int main() {
    std::cout << "********some test********" << std::endl;
    Package myPackage1("MyName", "RecName", "MyHome", "RecHome", "ChongQing", "HeBei", "ShaPingBa", "XingTai",
                       "054001",
                       "49256", 11, 5);
    cout << "Package 11 ounce, 5 dolor per ounce:\t" << myPackage1.calculateCost() << endl;
    TwoDayPackage myPackage2("MyName", "RecName", "MyHome", "RecHome", "ChongQing", "HeBei", "ShaPingBa", "XingTai",
                             "054001",
                             "49256", 11, 5, 4);
    cout << "Package 11 ounce, 5 dolor per ounce,4 dolor baseFee:\t" << myPackage2.calculateCost() << endl;
    OvernightPackage myPackage3("MyName", "RecName", "MyHome", "RecHome", "ChongQing", "HeBei", "ShaPingBa",
                                "XingTai",
                                "054001",
                                "49256", 11, 5, 3);
    cout << "Package 11 ounce, 5 dolor per ounce,3 dolor per ounce as additional fee:\t"
         << myPackage3.calculateCost()
         << endl;
    cout << "************************\n" << "test for wrong" << endl;
    try {
        Package worng1("MyName", "RecName", "MyHome", "RecHome", "ChongQing", "HeBei", "ShaPingBa", "XingTai",
                       "054001",
                       "49256", -10, 5);
    } catch (std::string e) {
        cout << e << endl;
    }
    try {
        OvernightPackage worng1("MyName", "RecName", "MyHome", "RecHome", "ChongQing", "HeBei", "ShaPingBa", "XingTai",
                                "054001",
                                "49256", 10, 5, -2);
    } catch (std::string e) {
        cout << e << endl;
    }
    try {
        TwoDayPackage worng1("MyName", "RecName", "MyHome", "RecHome", "ChongQing", "HeBei", "ShaPingBa", "XingTai",
                             "054001",
                             "49256", 10, 5, -6);
    } catch (std::string e) {
        cout << e << endl;
    }
    return 0;
}
