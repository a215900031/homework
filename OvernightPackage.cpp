//
// Created by zhu on 2017/12/6.
//

#include "OvernightPackage.h"

OvernightPackage::OvernightPackage(const string &sendName, const string &recName, const string &sendAdress,
                                   const string &recAdress, const string &sendCity, const string &recCity,
                                   const string &sendState, const string &recState, const string &sendZIP,
                                   const string &recZIP, double weight, double costPerOunce, double additionalFee) :
        Package(sendName, recName,
                sendAdress, recAdress,
                sendCity, recCity,
                sendState, recState,
                sendZIP, recZIP,
                weight, costPerOunce),
        _addtionalFee(additionalFee) {
    if (additionalFee < 0) {
        throw (std::string("error:additional is less than zero"));
    }
}

double OvernightPackage::calculateCost() {
    return (_addtionalFee + _costPerOunce) * _weight;
}
