//
// Created by zhu on 2017/12/6.
//

#include "TwoDayPackage.h"

TwoDayPackage::TwoDayPackage(const string &sendName, const string &recName, const string &sendAdress,
                             const string &recAdress, const string &sendCity, const string &recCity,
                             const string &sendState, const string &recState, const string &sendZIP,
                             const string &recZIP, double weight, double costPerOunce, double baseFee) :
        Package(sendName, recName,
                sendAdress, recAdress,
                sendCity, recCity,
                sendState, recState,
                sendZIP, recZIP,
                weight,
                costPerOunce),
        _baseFee(baseFee) {
    if (baseFee < 0) {
        throw (std::string("error:baseFee is less than zero"));
    }
}

double TwoDayPackage::calculateCost() {
    return _baseFee + _weight * _costPerOunce;
}
