//
// Created by zhu on 2017/12/6.
//

#ifndef HOMEWORK_TWODAYPACKAGE_H
#define HOMEWORK_TWODAYPACKAGE_H


#include "Package.h"

class TwoDayPackage: public Package{
public:
    TwoDayPackage(const string &sendName, const string &recName, const string &sendAdress,
                      const string &recAdress, const string &sendCity, const string &recCity,
                      const string &sendState, const string &recState, const string &sendZIP,
                      const string &recZIP, double weight, double costPerOunce, double baseFee);

    double calculateCost() override;
private:
    double _baseFee;
};


#endif //HOMEWORK_TWODAYPACKAGE_H
