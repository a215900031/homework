//
// Created by zhu on 2017/12/6.
//

#include "Package.h"

Package::Package(std::string sendName, std::string recName,
                 std::string sendAdress, std::string recAdress,
                 std::string sendCity, std::string recCity,
                 std::string sendState, std::string recState,
                 string sendZIP, string recZIP,
                 double weight, double costPerOunce)
        :
        _sendName(sendName), _recName(recName),
        _sendAdress(sendAdress), _recAdress(recAdress),
        _sendCity(sendCity), _recCity(recCity),
        _sendState(sendState), _recState(recState),
        _sendZIP(sendZIP), _recZIP(recZIP),
        _weight(weight), _costPerOunce(costPerOunce) {
    if (weight <= 0 || costPerOunce <= 0) {
        throw (std::string("\nerror:weight or costPerOunce is a negative number"));
    }
}

double Package::calculateCost() {
    return _weight * _costPerOunce;
}
