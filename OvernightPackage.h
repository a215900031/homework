//
// Created by zhu on 2017/12/6.
//

#ifndef HOMEWORK_OVERNIGHTPACKAGE_H
#define HOMEWORK_OVERNIGHTPACKAGE_H


#include "Package.h"

class OvernightPackage: public Package {
public:
    OvernightPackage(const string &sendName, const string &recName, const string &sendAdress,
                         const string &recAdress, const string &sendCity, const string &recCity,
                         const string &sendState, const string &recState, const string &sendZIP,
                         const string &recZIP, double weight, double costPerOunce, double additionalFee);
    double calculateCost() override;
protected:
    double _addtionalFee;
};


#endif //HOMEWORK_OVERNIGHTPACKAGE_H
