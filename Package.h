//
// Created by zhu on 2017/12/6.
//

#ifndef HOMEWORK_PACKAGE_H
#define HOMEWORK_PACKAGE_H

#include <string>

#define ll long long
using namespace std;

class Package {
public:

    Package(string sendName, string recName,
            string sendAdress, string recAdress,
            string sendCity, string recCity,
            string sendState, string recState,
            string sendZIP, string recZIP,
            double weight, double costPerOunce);

    virtual double calculateCost();

protected:
    double _weight;
    double _costPerOunce;

private:
    string _sendName;
    string _recName;
    string _sendAdress;
    string _recAdress;
    string _sendCity;
    string _recCity;
    string _sendState;
    string _recState;
    string _sendZIP;
    string _recZIP;
};


#endif //HOMEWORK_PACKAGE_H
